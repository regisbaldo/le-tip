import { createApp } from 'vue'
import { store } from './store';

import './assets/css/normalize.css'
import './assets/css/global.scss'

import App from './App.vue'

const app = createApp(App);
app.use(store)
app.mount('#app')