import axios from "axios";
export const baseUrl = "https://api.apilayer.com/";

const reqInstance = axios.create({
    baseURL: import.meta.env.VITE_CURRENCY_CONVERSION_BASE_URL
});

export function convert(from, to) {
    return reqInstance.get("/graphql", {
        params: {
            query: `query currencyConversion($base: String!, $convert: [String]!){
                            currencyConversion(baseCurrency: $base, convertCurrencies: $convert){
                                conversions{
                                    rate
                                }
                            }
                    }`,
            variables: {
                base: to,
                convert: from
            }

        }
    });
}

