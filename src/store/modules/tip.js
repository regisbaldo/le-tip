import { convert } from "../../services/convertCurrency";
import { tipCalculator } from "../../utils/tipCalculator";

export const tip = {
    namespaced: true,
    state() {
        return {
            currency: "EUR",
            bill: 0.00,
            tip: 10,
            dividedBy: 2,
            totalConverted: 0,
            conversionStatus: "done"
        }
    },
    getters: {
        getCurrency(state) {
            return state.currency;
        },
        getBill(state) {
            return state.bill;
        },
        getTip(state) {
            return state.tip;
        },
        getDividedBy(state) {
            return state.dividedBy;
        },
        getTotalConverted(state) {
            return parseFloat(state.totalConverted);
        },
        getConversionStatus(state) {
            return state.conversionStatus;
        },
        getTipCalculated(state) {
            return tipCalculator(parseFloat(state.bill), state.tip, state.dividedBy);
        }
    },
    mutations: {
        setBill(state, value) {
            state.bill = value
        },
        setCurrency(state, value) {
            state.currency = value
        },
        setTip(state, value) {
            state.tip = parseInt(value);
        },
        setDividedBy(state, value) {
            state.dividedBy = parseInt(value)
        },
        setConversionStatus(state, value) {
            state.conversionStatus = value;
        },
        setTotalConverted(state, value) {
            state.totalConverted = value;
        }

    },
    actions: {
        changeCurrency(context, currency) {
            if (currency === "USD") {
                context.commit('setCurrency', "EUR");
            } else {
                context.commit("setCurrency", "USD");
            }
        },
        convert(context) {
            context.commit('setConversionStatus', "inProgress");

            const tipCalculated = context.getters["getTipCalculated"];
            convert(context.state.currency, "BRL").then((response) => {
                context.commit("setTotalConverted", (tipCalculated.total * response.data.data.currencyConversion.conversions[0].rate).toFixed(2))
                context.commit('setConversionStatus', "done");
            }).catch((error) => {
                console.log(error);
                context.commit('setConversionStatus', "error");
            })
        }
    }
}