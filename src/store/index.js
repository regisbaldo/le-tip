import { createStore } from 'vuex'
import { tip } from './modules/tip'

export const store = createStore({
    modules: {
        tip
    }
})