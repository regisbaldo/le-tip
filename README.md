# Projeto Le/tip

## [Avaliação Técnica - Frontend Convenia](https://swop.cx/)
## Sobre o projeto

> O projeto consiste em uma aplicação SPA em Vue criada com [Vite](https://vitejs.dev/). O mesmo projeto consome a api em GraphQL ["Currency Conversion"](https://gitlab.com/convenia/assessments/currency-conversion) fornecida no desafio.
## Pacotes utilizados
### Depedências


Pacote   | Versão
--------- | ------
[axios](https://www.npmjs.com/package/axios) | ^0.27.2
[vue](https://www.npmjs.com/package/vue)  | ^3.2.37
[vuex](https://www.npmjs.com/package/vuex) | ^4.0.2
###  Depedências de desenvolvimento

Pacote   | Versão
--------- | ------
[@vitejs/plugin-vue](https://www.npmjs.com/package/@vitejs/plugin-vue)  | ^3.0.0
[sass](https://www.npmjs.com/package/sass)  | ^1.54.3
[vite](https://www.npmjs.com/package/vite) |^3.0.0

## Como rodar o projeto

>Para que o projeto funcione é preciso ter a versão do Node.js 14.18 ou superior.  

>Ter funcionando a API que irá ser consumida e colocar a URL dela na variável de ambiente "VITE_CURRENCY_CONVERSION_BASE_URL" no arquivo .env na raiz do projeto. (por padrão para o teste deixei na porta 3000 mas fiquem a vontade de colocar em outra porta).

### Comandos
>Para instalar as dependências do projeto
~~~javascript
npm install
~~~
>Para rodar o projeto em ambiente de desenvolvimento

~~~javascript
npm run dev
// por padrão irá ser criada na porta 5173

~~~
>Para fazer o build do projeto
~~~javascript
npm run build
~~~
>Para rodar o projeto em ambiente de produção
~~~javascript
npm run preview
// por padrão irá ser criada na porta 4173
~~~

